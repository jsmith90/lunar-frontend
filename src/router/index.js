import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/pages/Index'
import Booking from '@/components/pages/Booking'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/booking',
      name: 'Booking',
      component: Booking
    }
  ]
})
